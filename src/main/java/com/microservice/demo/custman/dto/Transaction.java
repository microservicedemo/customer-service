package com.microservice.demo.custman.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Transaction {
    @Id
    @GeneratedValue
    Long id;
    String description;
    Integer amount;
    Long customerId;
}
