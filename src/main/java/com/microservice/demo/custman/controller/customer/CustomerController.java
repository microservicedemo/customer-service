package com.microservice.demo.custman.controller.customer;

import com.microservice.demo.custman.model.customer.Customer;
import com.microservice.demo.custman.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("cust-man-api/customers")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @RequestMapping(method= RequestMethod.POST)
    public Customer save(@RequestBody Customer account) {
        return customerService.save(account);
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<Customer> findAll() {
        return customerService.findAll();
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.GET)
    public Customer findOne(@PathVariable("id") Long id) {
        return customerService.findOne(id);
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        try {
            customerService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(500).build();
        }
    }

}
