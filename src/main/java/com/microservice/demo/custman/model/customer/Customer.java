package com.microservice.demo.custman.model.customer;

import com.microservice.demo.custman.dto.Transaction;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Getter
@Setter
@Entity
public class Customer {
    @Id
    @GeneratedValue
    Long id;
    String name;
    int age;
    @Transient
    List<Transaction> transactionList;
}
