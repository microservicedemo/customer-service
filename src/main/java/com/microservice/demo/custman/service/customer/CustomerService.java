package com.microservice.demo.custman.service.customer;

import com.microservice.demo.custman.dto.Transaction;
import com.microservice.demo.custman.feignclient.AccountClient;
import com.microservice.demo.custman.model.customer.Customer;
import com.microservice.demo.custman.repository.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    @Autowired
    AccountClient accountClient;

    @Autowired
    CustomerRepository customerRepository;

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public List<Customer> findAll() {
        ArrayList<Customer> customers = new ArrayList<>();
        Iterable<Customer> customersIt = customerRepository.findAll();
        customersIt.forEach(customer -> {
            List<Transaction> transactions = accountClient.findAllByCustomer(customer.getId());
            customer.setTransactionList(transactions);
            customers.add(customer);
        });


        return customers;
    }

    public Customer findOne(Long id) {
        return customerRepository.findById(id).get();
    }

    public void delete(Long id) {
        Customer account = new Customer();
        account.setId(id);
        customerRepository.delete(account);
    }
}
