package com.microservice.demo.custman.feignclient;

import com.microservice.demo.custman.dto.Transaction;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Component
@FeignClient("account-service")
public interface AccountClient {

    @RequestMapping(value = "/acc-api/transactions/customer/{customerId}", method = RequestMethod.GET)
    List<Transaction> findAllByCustomer(@PathVariable("customerId") Long customerId);
}
